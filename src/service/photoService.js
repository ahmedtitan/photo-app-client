import axiosInstance from '.';

export const getAllPhotos = async () => {
  try {
    const response = await axiosInstance.get('/photo');
    return Promise.resolve(response.data.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getUserPhotos = async (authorID) => {
  try {
    const response = await axiosInstance.get(`/photo/${authorID}`);
    return Promise.resolve(response.data.data);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const saveUserPhotos = async (photos) => {
  try {
    const response = await axiosInstance.put('/photo', photos);
    return Promise.resolve(response.data.data);
  } catch (error) {
    return Promise.reject(error);
  }
};
