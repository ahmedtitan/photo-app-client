import axios from 'axios';

// ----------------------------------------------------------------------

const DOMAIN = 'http://localhost:4000';
const BASE_URL = '/api/v1/pa';

const axiosInstance = axios.create({
  baseURL: DOMAIN + BASE_URL,
  timeout: 20000,
});

export default axiosInstance;
