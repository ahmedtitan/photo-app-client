import React from 'react';
import './tabs.css';
import PropTypes from 'prop-types';

const Tabs = ({ value, setValue, tabs }) => {
  return (
    <div className="mainTabContainer">
      {tabs.map((tab, index) => (
        <button onClick={() => setValue(index)} className={index === value ? 'mainTabButtonSelected' : 'mainTabButton'}>
          {tab.tabTitle}
        </button>
      ))}
    </div>
  );
};

Tabs.propTypes = {
  value: PropTypes.number,
  setValue: PropTypes.func,
  tabs: PropTypes.array,
};

export default Tabs;
