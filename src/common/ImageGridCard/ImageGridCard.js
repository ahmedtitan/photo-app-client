import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';

const ImageGridCard = ({ photo, text }) => {
  return (
    <Card>
      <img src={`${photo}/650/410`} alt="imgCard" />
      {text && (
        <div className="imageGridTextWrapper">
          <p className="imageGridText">{text}</p>
        </div>
      )}
    </Card>
  );
};

ImageGridCard.propTypes = {
  photo: PropTypes.object,
  text: PropTypes.string,
};

export default ImageGridCard;
