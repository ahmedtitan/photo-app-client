import React from 'react';
import './grid.css';
import PropTypes from 'prop-types';
import ImageGridCard from '../ImageGridCard/ImageGridCard';

const Grid = ({ data, isGridNumberEnabled }) => {
  return (
    <div className="gridContainer">
      {data.map((item, index) => (
        <div className="gridItem">
          <ImageGridCard photo={item} text={isGridNumberEnabled ? `${index + 1}/${data.length}` : null} />
        </div>
      ))}
    </div>
  );
};

Grid.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  isGridNumberEnabled: PropTypes.bool,
};

export default Grid;
