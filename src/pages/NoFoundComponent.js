import React from 'react';

const NoFoundComponent = () => {
  return <h3>404 Page not found.</h3>;
};

export default NoFoundComponent;
