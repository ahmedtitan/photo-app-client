import React from 'react';
import TabPanel from '../common/TabPanel/TabPanel';
import ViewPhoto from '../components/ViewPhoto';
import EditPhoto from '../components/EditPhoto';
import { ImageProvider } from '../contexts/imageContext';
import Tabs from '../common/Tabs/Tabs';

const tabs = [
  {
    tabIndex: 0,
    tabTitle: 'View Grid',
  },
  {
    tabIndex: 1,
    tabTitle: 'Edit Grid',
  },
];

const Home = (props) => {
  const [value, setValue] = React.useState(0);

  return (
    <ImageProvider>
      <Tabs value={value} setValue={setValue} tabs={tabs} />
      <TabPanel value={value} index={0}>
        <ViewPhoto />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <EditPhoto />
      </TabPanel>
    </ImageProvider>
  );
};

export default Home;
