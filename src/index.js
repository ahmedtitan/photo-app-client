import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { SnackbarProvider } from 'notistack';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';

// Auth context
import { AuthProvider } from './contexts/authContext';

ReactDOM.render(
  <React.StrictMode>
    <AuthProvider>
      <SnackbarProvider maxSnack={3}>
        <DndProvider backend={HTML5Backend}>
          <App />
        </DndProvider>
      </SnackbarProvider>
    </AuthProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
reportWebVitals();
