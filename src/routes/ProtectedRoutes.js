import React from 'react';
import { lazy } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Suspense } from 'react';

const routes = [
  {
    path: 'home',
    component: lazy(() => import('../pages/Home')),
    exact: true,
  },
];

const ProtectedRoutes = (props) => {
  return (
    <Switch>
      <Suspense fallback={<p>Loading...</p>}>
        {routes.map(({ component: Component, path, exact }) => (
          <Route path={`/${path}`} key={path} exact={exact}>
            <Component />
          </Route>
        ))}
      </Suspense>
    </Switch>
  );
};

ProtectedRoutes.propTypes = {};

export default ProtectedRoutes;
