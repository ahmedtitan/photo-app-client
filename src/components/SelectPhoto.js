import React, { useContext } from 'react';
import Grid from '@mui/material/Grid';
import ImageCard from './ImageCard';
import { ImageContext } from '../contexts/imageContext';
import { useSnackbar } from 'notistack';

const SelectPhoto = ({ photos }) => {
  const useImage = () => useContext(ImageContext);
  const { update, selectedImages } = useImage();
  const { enqueueSnackbar } = useSnackbar();

  const handleImageSelect = (selected, remove) => {
    if (selectedImages.length < 9 || remove)
      remove ? update(selectedImages.filter((photo) => photo !== selected)) : update([...selectedImages, selected]);
    else
      enqueueSnackbar('Image selection limit reached', {
        variant: 'warning',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      });
  };

  return (
    <div className="selectedPhotoWrapper">
      {!!photos.length &&
        photos?.map((photo, index) => (
          <ImageCard
            index={index}
            photo={photo}
            handleImageSelect={handleImageSelect}
            selectedImages={selectedImages}
          />
        ))}
    </div>
  );
};

export default SelectPhoto;
