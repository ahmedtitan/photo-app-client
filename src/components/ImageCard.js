import React from 'react';
import PropTypes from 'prop-types';
import Card from '../common/Card/Card';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea } from '@mui/material';

const ImageCard = ({ index, handleImageSelect, selectedImages }) => {
  const id = index * 4;
  const isSelected = selectedImages.some((image) => image === `https://picsum.photos/id/${id}`);
  return (
    <div className={isSelected ? 'imageCardWrapperSelected' : 'imageCardWrapper'}>
      <Card>
        <CardActionArea onClick={() => handleImageSelect(`https://picsum.photos/id/${id}`, isSelected)}>
          <CardMedia
            sx={{ opacity: isSelected ? 0.25 : 1 }}
            component="img"
            height="200"
            image={`https://picsum.photos/id/${id}/200/300`}
          />
        </CardActionArea>
      </Card>
    </div>
  );
};

ImageCard.propTypes = {
  index: PropTypes.number,
  handleImageSelect: PropTypes.func,
  selectedImages: PropTypes.array,
};

export default ImageCard;
