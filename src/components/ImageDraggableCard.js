import React from 'react';
import PropTypes from 'prop-types';
import Card from '../common/Card/Card';
import { CardMedia } from '@mui/material';
import ImageGridCard from '../common/ImageGridCard/ImageGridCard';

const ImageDraggableCard = ({ photo }) => {
  return (
    <Card>
      <CardMedia sx={{ width: '100%', height: '10vw' }} component="img" image={`${photo}/400/200`} />
    </Card>
  );
};

ImageDraggableCard.propTypes = {
  selectedImages: PropTypes.array,
};

export default ImageDraggableCard;
