import React, { useEffect, useState, useContext } from 'react';

import { getAllPhotos, getUserPhotos } from '../service/photoService';
import HorizontalLinearStepper from './HorizontalLinearStepper';
import { ImageContext } from '../contexts/imageContext';

// MUI components
import CircularProgress from '@mui/material/CircularProgress';
import Container from '@mui/material/Container';

const EditPhoto = (props) => {
  const useImage = () => useContext(ImageContext);
  const { update } = useImage();

  const [photos, setPhotos] = useState([]);
  const [selectedPhotos, setSelectedPhotos] = useState([]);
  const [isLoading, setisLoading] = useState(false);
  const [authorID, setAuthorID] = useState('');

  useEffect(() => {
    const fetchAllPhotos = async () => {
      try {
        setisLoading(true);
        const photos = await getAllPhotos();
        setPhotos(photos);
        setAuthorID(photos?.author?.id);
        setisLoading(false);
      } catch (error) {
        setisLoading(false);
      }
    };
    const fetchUserPhotos = async () => {
      try {
        const response = await getUserPhotos('101');
        update(response.map((photo) => photo.imageUrl));
      } catch (error) {
        console.log(error);
      }
    };
    fetchAllPhotos();
    fetchUserPhotos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {isLoading ? (
        <Container sx={{ textAlign: 'center', padding: '50px' }}>
          <CircularProgress size={80} />
        </Container>
      ) : (
        <HorizontalLinearStepper
          photos={photos.entries || []}
          setSelectedPhotos={setSelectedPhotos}
          selectedPhotos={selectedPhotos}
          authorID={authorID}
        />
      )}
    </>
  );
};

export default EditPhoto;
