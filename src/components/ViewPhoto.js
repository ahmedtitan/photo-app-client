import React, { useEffect, useState, useCallback } from 'react';
import { getUserPhotos } from '../service/photoService';
import { useSnackbar } from 'notistack';
import Grid from '../common/Grid/Grid';

// ui components
import CircularProgress from '@mui/material/CircularProgress';

const ViewPhoto = () => {
  const { enqueueSnackbar } = useSnackbar();

  const [photos, setphotos] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchUserPhotos = useCallback(async () => {
    try {
      setIsLoading(true);
      const response = await getUserPhotos('101');
      setphotos(response);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      enqueueSnackbar('Unable to fetch the photos. Please try again', {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      });
    }
  }, [enqueueSnackbar]);

  useEffect(() => {
    fetchUserPhotos();
  }, [fetchUserPhotos]);

  return (
    <div>
      {isLoading && (
        <div className="photoViewMainDiv">
          <CircularProgress size={80} />
        </div>
      )}
      {!isLoading && photos.length ? (
        <Grid data={photos.map((p) => p.imageUrl)} />
      ) : (
        <div className="photoViewMainDiv">
          <h3>Photo grid is empty. Please go to Edit Grid tab and create a photo grid.</h3>
        </div>
      )}
    </div>
  );
};

export default ViewPhoto;
