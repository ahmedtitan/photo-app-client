import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Typography from '@mui/material/Typography';
import SelectPhoto from './SelectPhoto';
import { ImageContext } from '../contexts/imageContext';
import ArrangeGrid from './ArrangeGrid';
import { useSnackbar } from 'notistack';
import { saveUserPhotos } from '../service/photoService';

const HorizontalLinearStepper = ({ photos, authorID }) => {
  const { enqueueSnackbar } = useSnackbar();
  const useImage = () => useContext(ImageContext);
  const { selectedImages } = useImage();

  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSave = () => {
    try {
      if (selectedImages.length) {
        saveUserPhotos({
          authorID,
          photos: selectedImages.map((photo, index) => ({ imageUrl: photo, photoSequence: index })),
        });
        return enqueueSnackbar('Saved succefully', {
          variant: 'success',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        });
      } else
        enqueueSnackbar('Please select at least one photo to save', {
          variant: 'warning',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        });
    } catch (error) {
      enqueueSnackbar('photos saved', {
        variant: 'info',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      });
    }
  };

  return (
    <div>
      <Stepper activeStep={activeStep}>
        <Step key={'Select Photos'}>
          <StepLabel>Select Photos</StepLabel>
        </Step>
        <Step key={'Arrange Grid'}>
          <StepLabel>Arrange Grid</StepLabel>
        </Step>
      </Stepper>
      <div className="stepperContainer">
        {activeStep === 0 && <SelectPhoto photos={photos} />}
        {activeStep === 1 && <ArrangeGrid />}
      </div>
      <React.Fragment>
        <div className="stepperDiscription">
          <Typography sx={{ mt: 2, mb: 1 }}>
            {activeStep === 0 ? 'Please select 9 images' : 'Please set the image order. Left side images are draggable'}
          </Typography>
          <Typography sx={{ mt: 2, mb: 1 }}>Selected Images: {selectedImages.length}/9</Typography>
        </div>
        <div className="stepperButtons">
          <button disabled={activeStep === 0} onClick={handleBack}>
            Back
          </button>
          <button onClick={activeStep === 2 - 1 ? handleSave : handleNext}>
            {activeStep === 2 - 1 ? 'Save' : 'Next'}
          </button>
        </div>
      </React.Fragment>
    </div>
  );
};

HorizontalLinearStepper.propTypes = {
  photos: PropTypes.array,
  authorID: PropTypes.string,
};

export default HorizontalLinearStepper;
