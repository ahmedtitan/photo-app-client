import { useCallback, useContext } from 'react';
import { Card } from './Card';
import update from 'immutability-helper';
import { ImageContext } from '../../contexts/imageContext';

export const Container = () => {
  const useImage = () => useContext(ImageContext);
  const { selectedImages, update: updateImageorder } = useImage();

  {
    const moveCard = useCallback(
      (dragIndex, hoverIndex) => {
        const dragCard = selectedImages[dragIndex];
        updateImageorder(
          update(selectedImages, {
            $splice: [
              [dragIndex, 1],
              [hoverIndex, 0, dragCard],
            ],
          }),
        );
      },
      [selectedImages, updateImageorder],
    );
    const renderCard = (photo, index) => {
      return <Card key={photo} index={index} id={photo} text={photo} moveCard={moveCard} photo={photo} />;
    };
    return (
      <>
        <div className="dndContainer">{selectedImages.map((photo, i) => renderCard(photo, i))}</div>
      </>
    );
  }
};
