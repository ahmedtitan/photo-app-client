import React, { useContext } from 'react';
import { ImageContext } from '../contexts/imageContext';

import { Container } from './React-Dnd/Container';
import Grid from '../common/Grid/Grid';

// ui components
import Box from '@mui/material/Box';

const ArrangeGrid = () => {
  const useImage = () => useContext(ImageContext);
  const { selectedImages } = useImage();

  return (
    <div className="arrangeGridContainerWrapper">
      <Container />
    </div>
  );
};

export default ArrangeGrid;
