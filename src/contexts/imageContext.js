import { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';

// ----------------------------------------------------------------------

const initialState = {
  selectedImages: [],
};

const handlers = {
  INITIALIZE: () => {
    return {
      selectedImages: [],
    };
  },

  UPDATE: (state, action) => {
    const { photos } = action.payload;
    return {
      selectedImages: photos,
    };
  },
};

const reducer = (state, action) => (handlers[action.type] ? handlers[action.type](state, action) : state);

const ImageContext = createContext({
  ...initialState,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
});

ImageProvider.propTypes = {
  children: PropTypes.node,
};

function ImageProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch({
      type: 'INITIALIZE',
    });
  }, []);

  const update = async (photos) => {
    dispatch({
      type: 'UPDATE',
      payload: {
        photos,
      },
    });
  };

  return (
    <ImageContext.Provider
      value={{
        ...state,
        update,
      }}
    >
      {children}
    </ImageContext.Provider>
  );
}

export { ImageContext, ImageProvider };
