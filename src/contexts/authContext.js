import { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';

// ----------------------------------------------------------------------

const initialState = {
  isAuthenticated: false,
};

const handlers = {
  INITIALIZE: (action) => {
    const isAuthenticated = window.localStorage.getItem('isAuthenticated');

    return {
      isAuthenticated,
    };
  },

  LOGIN: () => ({
    isAuthenticated: true,
  }),
  LOGOUT: () => ({
    isAuthenticated: false,
  }),
};

const reducer = (state, action) => (handlers[action.type] ? handlers[action.type](state, action) : state);

const AuthContext = createContext({
  ...initialState,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
});

AuthProvider.propTypes = {
  children: PropTypes.node,
};

function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch({
      type: 'INITIALIZE',
    });
  }, []);

  const login = async () => {
    localStorage.setItem('isAuthenticated', true);
    dispatch({ type: 'LOGIN' });
  };

  const logout = async () => {
    localStorage.setItem('isAuthenticated', false);
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        login,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export { AuthContext, AuthProvider };
