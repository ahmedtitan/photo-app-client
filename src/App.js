import { useContext, Suspense } from 'react';
import './app.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SignIn from './pages/SignIn';
import PublicRoute from './routes/PublicRoute';
import { AuthContext } from './contexts/authContext';
import PrivateRoute from './routes/PrivateRoute';
import ProtectedRoutes from './routes/ProtectedRoutes';
import NoFoundComponent from './pages/NoFoundComponent';

function App() {
  const useAuth = () => useContext(AuthContext);
  const { isAuthenticated } = useAuth();

  return (
    <Router>
      <Suspense fallback={<p>Loading...</p>}>
        <Switch>
          <PublicRoute exact={true} path="/sign-in" isAuthenticated={isAuthenticated}>
            <SignIn />
          </PublicRoute>
          <PrivateRoute path="/" isAuthenticated={isAuthenticated}>
            <ProtectedRoutes />
          </PrivateRoute>
          <Route path="*">
            <NoFoundComponent />
          </Route>
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
