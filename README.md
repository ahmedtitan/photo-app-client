# How to run the project

## STEP 1

Install all dependencies

> npm install

## STEP 2

> npm start

### How to use the app

This is a very simple app whick contains only 2 routes and 2 main functionality

1. Run the backend
2. Signin page will load initially.

> username: admin
> password: admin

3. View Grid tab will not have any data initilly
4. Go to edit grid
5. Select images and click next
6. Users can re-order the grid by drag and drop the images fron the left panel
7. Click save
8. Got to view grid tab
9. Grid will be created according to users selection

### code explaination and used packages

This project contains only 2 routes.

1. sign-in
2. home

- Authorization part is handle by just a variable stored in localstorage.
- Username and password is hardcoded.
- Mteril ui latest version is used.
- Material UI sx property used for styling.
- React context api used.
- Axios is used for http requests.
- Backend port is hardcoded: loclhost:4000

### Docker run

Docker file added.
Docker container port: 80

Create docker container

> docker build -t photo-app-clien:latest .

Run docker container

> docker run -it -d -p 3000:80 photo-app-clien:latest
